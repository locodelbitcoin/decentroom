/*
* Copyright 2013 Google Inc.
* Copyright 2014 Andreas Schildbach
* Copyright 2018 Enrique Melero, Felipe Santi
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

//package com.backmagicbox.core;

import org.bitcoinj.core.*;
import org.bitcoinj.crypto.KeyCrypterException;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.utils.BriefLogFormatter;
import org.bitcoinj.wallet.Wallet;
import org.bitcoinj.wallet.listeners.WalletCoinsReceivedEventListener;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;

import org.slf4j.*;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * ForwardingService demonstrates basic usage of the library.
 * It sits on the network and when it receives coins,
 * simply sends them onwards to an address given on the command line.
 */

public class DecentRoom {

  // logger helper object
  final static Logger logger = LoggerFactory.getLogger(DecentRoom.class);
  private static byte[] roomAddressHash160;
  // Bitcoin address to which incoming bitcoins
  // in the hotel room will be forwarded
  private static Address forwardingAddress;
  // list of change addresses created in forwarding transactions
  private static List<byte[]> changeAddresses = new ArrayList<byte[]>();
  // object that contains all Bitcoin wallet functions
  private static WalletAppKit kit;
  // helper object for Bitcoin network management
  private static NetworkParameters params;

  /**
   * Program starting point
   * command line parameters:
   * - forwardingAddress: address to which forward incoming Bitcoins to the room
   * - "testnet": this is optional, by default the program will use mainnet
   * e.g. Command Line Interface call (mimicks $ make start):
   * java -classpath ./:./libs/bitcoinj-core-0.14.7-bundled.jar:./libs/slf4j-simple-1.7.25.jar:./libs/slf4j-api-1.7.25.jar DecentRoom 2MvDt93dupLUXef6nQZ8usdPUXdppbzLxpZ testnet
   *
   */

  public static void main(String[] args) {
    // This line makes the log output more compact and easily read,
    // especially when using the JDK log adapter.
    // BriefLogFormatter.init();

    // Check the return address has been supplied as parameter
    if ((args.length < 1) || ((args.length == 1) && args[0].equals("testnet"))) {
    System.err.println("ERROR: MISSING PARAMETER [address-to-send-back-to]");
    return;
    }

    // Figure out which network we should connect to.
    // Each one gets its own set of files.
    String filePrefix;
    if (args.length > 1 && args[1].equals("testnet")) {
      params = TestNet3Params.get();
      filePrefix = "decentroom-testnet";
    } else {
      params = MainNetParams.get();
      filePrefix = "decentroom";
    }

    // Parse the address given as the first parameter.
    forwardingAddress = Address.fromBase58(params, args[0]);

    // Start up a basic app using a class that automates some boilerplate.
    kit = new WalletAppKit(params, new File(System.getProperty("user.home")), filePrefix) {
        @Override
        protected void onSetupCompleted () {
          // Allow users to spend Bitcoins even if the incoming transaction is unconfirmed
          kit.wallet().allowSpendingUnconfirmedTransactions();
        }
    };

    // Download the block chain and wait until it's done.
    // This is a SPV Wallet (Simplified Payment Verification)
    // All block headers will be downloaded in order to validate the payments
    kit.startAsync();
    kit.awaitRunning();

    // After initializing itself, the wallet will create its own address
    // This address is by definition the hotel room address to which customers pay
    // Bitcoin address of the hotel room
    Address roomAddress;
    roomAddress = kit.wallet().currentReceiveKey().toAddress(params);
    roomAddressHash160 = roomAddress.getHash160();

    // Add a callback function to monitor the reception of money
    // This works even if the hotel room has been disconnected from a Bitcoin node for some time
    kit.wallet().addCoinsReceivedEventListener(new WalletCoinsReceivedEventListener() {
        @Override
        public void onCoinsReceived(Wallet w, final Transaction tx, Coin prevBalance, Coin newBalance) {
            // Runs in the dedicated "user thread" (see bitcoinj docs for more info on this).
            // The transaction "tx" can either be pending, or included into a block (we didn't see the broadcast).
            Coin value = tx.getValueSentToMe(w);
            logger.info("Received unconfirmed transaction for " + value.toFriendlyString() + ": " + tx);
            boolean forwardThis = true;
            List<TransactionOutput> outputs = tx.getOutputs();
            byte[] tmpHash160;

            // Mechanism to not forward change
            for(TransactionOutput out : outputs) {
                tmpHash160 = out.getAddressFromP2PKHScript(params).getHash160();
                if (changeAddresses.contains(tmpHash160)) {
                    forwardThis = false;
                }
            }

            // Stop here if we don't want to forward coins
            if (!forwardThis) {
                logger.info("Bitcoins will be kept ");
                return;
            }

            // Otherwise, forward the coins to the address passed in parameter
            logger.info("Forwarding the coins to " + forwardingAddress);
            forwardCoins(tx, forwardingAddress);

            // Wait until it's made it into the block chain (may run immediately if it's already there).
            //
            // For this dummy app of course, we could just forward the unconfirmed transaction. If it were
            // to be double spent, no harm done. Wallet.allowSpendingUnconfirmedTransactions() would have to
            // be called in onSetupCompleted() above. But we don't do that here to demonstrate the more common
            // case of waiting for a block.
            Futures.addCallback(tx.getConfidence().getDepthFuture(1), new FutureCallback<TransactionConfidence>() {
                @Override
                public void onSuccess(TransactionConfidence result) {
                    // Uncomment this if you want to forward only confirmed transactions
                    // In such case, comment the line above forwardCoins(tx, forwardingAddress);
                    // forwardCoins(tx, forwardingAddress);
                    logger.info("Incoming transaction was confirmed: " + tx);
                }
                @Override
                public void onFailure(Throwable t) {
                    // This should never happen
                    logger.error("ERROR - onFailure method - please check");
                    throw new RuntimeException(t);
                }
            });
        } // callback end
    }); // listener end

    // Now that the callback to handle incoming money has been set up,
    // Give the hotel room public address to the hotel customer so that he/she can pay
    logger.info("\n" + "**********************************" + "\n" +
        "*** PAY THE ROOM TO THIS ADDRESS: " + roomAddress + "\n" + "**********************************" + "\n");
    logger.info("\n" + "**********************************" + "\n" +
        "*** QR CODE IS: " + "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=" +
        roomAddress + "\n" + "**********************************" + "\n");
    logger.info("Waiting for coins to arrive. Press Ctrl-C to quit.");
    // TODO: uncomment this later
    /*
    Runtime runtime = Runtime.getRuntime();
    Process process;
    try {

        // process = runtime.exec("node ../decentroom-bluetooth/leds.js red");
    } catch (IOException e) {
        // This should never happen
        logger.error("ERROR - onFailure method - please check");
        throw new RuntimeException(e);
    }
    */

    try {
      // SLEEP from here - program will wake up upon events thrown by WalletCoinsReceivedEventListener
      Thread.sleep(Long.MAX_VALUE);
    } catch (InterruptedException ignored) {
        Thread.currentThread().interrupt();  // set interrupt flag
        logger.info("Program finished.");
    }
  }

  /**
  * This method forwards an incoming transaction to any given address
  * @param tx incoming Bitcoin transaction
  * @param addressToForward public address to which Bitcoins will be forwarded
  */

  private static void forwardCoins(Transaction tx, Address addressToForward) {
    try {
      Coin value = tx.getValueSentToMe(kit.wallet());
      // Now send the coins back! Send with a small fee attached to ensure rapid confirmation.
      final Coin amountToSend = value.subtract(Transaction.REFERENCE_DEFAULT_MIN_TX_FEE);
      // Execute the forwarding transaction
      final Wallet.SendResult sendResult = kit.wallet().sendCoins(kit.peerGroup(), addressToForward, amountToSend);
      // Ensure sendResult is an object
      checkNotNull(sendResult);
      // Store the change address
      saveRestAddress(sendResult.tx);
      logger.info("Forwarding " + value.toFriendlyString());
      // Register a callback that is invoked when the transaction has propagated across the network.
      // This shows a second style of registering ListenableFuture callbacks, it works when you don't
      // need access to the object the future returns.
      sendResult.broadcastComplete.addListener(new Runnable() {
        @Override
        public void run() {
          // The wallet has changed now, it'll get auto saved shortly or when the app shuts down.
          System.out.println("Coins forwarded successfully! Transaction hash is " + sendResult.tx.getHashAsString());
          Runtime runtime = Runtime.getRuntime();
        }
      }, MoreExecutors.sameThreadExecutor());
    } catch (KeyCrypterException | InsufficientMoneyException e) {
      // This should never happen
      logger.error("ERROR - onFailure method - please check");
      throw new RuntimeException(e);
    }
  }

  /**
  * Save the change address of the forwarding transaction
  * in order to avoid forwarding the change, only what was paid to the room
  * @param tx The transaction built to forward money to forwardingAddress
  */
  private static void saveRestAddress(Transaction tx) {
    List<TransactionOutput> outputs = tx.getOutputs();
    byte[] tmpHash160;
    for(TransactionOutput out : outputs) {
          tmpHash160 = out.getAddressFromP2PKHScript(params).getHash160();
      // If the transaction output address is not the one of the room,
      // then add it to the list of change addresses
      if (!tmpHash160.equals(roomAddressHash160))
      {
        changeAddresses.add(tmpHash160);
        logger.info("Registering change address: " + tmpHash160);
      }
    }
  }
}
