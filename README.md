# DecentRoom
A simple bitcoin forward machine using Bitcoinj library

# Utilities

## Connect via SSH to the Raspberry Pi

SSH is enabled in your Raspberry Pi, you just need to use the ssh command in your machine.
The password for connecting is: `raspberry` (this is the default password, so you should change it).
Tutorial: https://www.digitalocean.com/community/tutorials/how-to-use-ssh-to-connect-to-a-remote-server-in-ubuntu

## Generate a QR Code

This web service generates QR codes.
Just replace what comes after data= (e.g. mjEaBQkYJq9peZuZqdZh1agJqv6zbBQn43) with your own address.
https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=mjEaBQkYJq9peZuZqdZh1agJqv6zbBQn43
