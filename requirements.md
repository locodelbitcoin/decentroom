# Introduction
Ms Nakamotel has a cosy and well located hotel in the center of the city. He does not have time or money to have a reservations system, deal with payments or with arrivals and departures of her clients.
This is why she has contacted us. She seems to have very clear requirements to make this room fully self-managed in a decentralized manner.

# Requirements (Customer experience)
Rooms should have an indication of whether they are available to be occupied or not. Probably a green light on the top of the door can be a good way to show it is ready to be occupied.

## Arrival
When a guest comes to the hotel and finds a room that is ready to be occupied the client can go there and make a payment to the room using bitcoins.

## How to make the payment?
The client goes in front of the door and opens an application that shows him the Room number and the Bitcoin address to which the payment is to be made.
Only rooms that are available to be occupied will show their address. At this point the client can make the payment. Once this payment has been received
the room turns off the green led and a red led switches on

## Opening the door
At this point only the user that has paid the room should be able to open it.
The client uses a mobile app to open the door. This is done by challenging the user to sign a nonce with the private key used to make the payment.
Once the challenge is shown to the user the yellow light switches on, indicating that the user is requested to proof her identity.
Once she has proved her identity responding to the challenge, the door opens and the yellow light switches off.
If the customer is not able to proof her identity within 5 minutes the yellow light switches off. The customer will have to request another
identity verification so she can open the room.


# Requirements (For Ms Nakamotel)
Hote manager wants that the room forwards the payments done to them as soon as they are made
The problem is how to define this address. For now this address will be given on the command line when the decentroom app is launched.

# Summary of states

- Green -> Free room, ready to be paid
- Red -> Room paid (client is in the room or not)
- Red & Yellow -> Room paid, user requested to give proof of identity within the next 5 minutes.


# Additional Requirements
## Room Booking
User could be able to make a payment to book the room for a future date

## Extending the Stay
User could pay additional amount to the room so that the stay is extended

## Room price and current status
The room could provide the user with information such as
- Date and time at which I have to leave the room
- Price of the room
- Availability

## Customer Identification
Customer could be requested to provide her/his name before the room is occupied
