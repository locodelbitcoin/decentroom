# Download bitcoinj-core and slf4j libraries from these links:
# General Bitcoinj project: https://bitcoinj.github.io/
# Link to Bitcoinj JAR file: https://search.maven.org/remotecontent?filepath=org/bitcoinj/bitcoinj-core/0.14.7/bitcoinj-core-0.14.7-bundled.jar
# General SLF4J project: https://www.slf4j.org/download.html

# EDIT THIS LINE TO ADD THE HOTEL FORWARDING ADDRESS
HOTEL_FORWARDING_ADDRESS=2MvDt93dupLUXef6nQZ8usdPUXdppbzLxpZ
#

VERSION=1.0
XLINT=-Xlint
CLASSPATH= ./:./libs/bitcoinj-core-0.14.7-bundled.jar:./libs/slf4j-simple-1.7.25.jar:./libs/slf4j-api-1.7.25.jar
TARGET=DecentRoom.class

default: all

%.class: %.java
	javac $(XLINT) -classpath $(CLASSPATH) $^

all: $(TARGET)

.PHONY: clean start

start:
	java -classpath $(CLASSPATH) DecentRoom $(HOTEL_FORWARDING_ADDRESS) testnet

clean:
	rm -f *~ *.class *.tmp $(TARGET)
